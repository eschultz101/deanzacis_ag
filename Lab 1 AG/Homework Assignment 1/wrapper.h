/**
   Wrapper Functions:
         xMalloc/xRealloc/xCalloc/xFopen/xFclose
        ... all of the above use eXit in case of error
*/
void *xMalloc(size_t size);
void *xRealloc(void *ptr, size_t numMembers);
void *xCalloc(size_t numMembers, size_t size);
FILE *xFopen(char *file, char *mode);
void  xFclose(FILE *fp);
