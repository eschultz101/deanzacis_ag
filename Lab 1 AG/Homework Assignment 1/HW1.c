/*********************************************************************************
** CIS 26B  Spring, 2013
** Advanced C
**********************************************************************************
**
** Homework 1: Review
**        Arrays, Pointers, Strings, Structures, and
**        Dynamic Allocation of Memory
** 
** Program accepts a file of state/cities and their high temperatures, then 
** calculates the average high temperature for each state/city and writes to file.
** 
**********************************************************************************
**
**  Written By: Eric Schultz
**
**  Date: 4/17/2013
**********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "wrapper.h"

#ifdef _MSC_VER
#include <crtdbg.h>
#endif

#define BSIZE 5

typedef struct city {
    char* name;
    int temperature;
} CITY;

CITY* getData(int* actualSize, char inputName[]);
void insertionSort(CITY* cityList, int actualSize);
void printList(CITY* cityList, int actualSize);
void calcAvgs(CITY* cityList, int actualSize, char outputName[]);
void destroyList(CITY* cityList, int actualSize);

int main( void ) {

    CITY* cityList;
    char inputName[25];
    char outputName[25];
    int actualSize;
    
    printf("Enter name of input file: ");
    scanf("%s", &inputName);
    
    cityList = getData(&actualSize, inputName);
    insertionSort(cityList, actualSize);
    printList(cityList, actualSize);
    printf("\n\nEnter name for output file (use .txt): ");
    scanf("%s", &outputName);
    calcAvgs(cityList, actualSize, outputName);
    destroyList(cityList, actualSize);

	printf("\n");
    
    #ifdef _MSC_VER
    printf( _CrtDumpMemoryLeaks() ? "Memory Leak\n" : "No Memory Leak\n");
    #endif

    system("pause");

    return 0;
}

CITY* getData(int* actualSize, char inputName[]) {
    /* function that accepts the variable for the size of the not yet existing city array
	and the string of the input file in order to create and return the cityList array. */
    CITY* cityList = NULL;
    int tTemp;
    char tName[100];
    FILE* fpIn = fopen(inputName, "r");
    int arrSize = BSIZE;
    int i = 0;
    cityList = (CITY*)xMalloc(BSIZE*sizeof(CITY));
    
    while(fscanf(fpIn, "%[^:] %*c %d", &tName, &tTemp) != EOF) {
        if(i < arrSize) {
            cityList[i].temperature = tTemp;
            cityList[i].name = (char*)xMalloc((strlen(tName) + 1)*sizeof(char));
            strcpy(cityList[i].name, tName);
            i++;
        }
        else {
            //using xRealloc to add BSIZE city slots to cityList if too small
            arrSize += BSIZE;
            cityList = xRealloc(cityList, arrSize*(sizeof(CITY)));
            cityList[i].temperature = tTemp;
            cityList[i].name = (char*)xMalloc((strlen(tName) + 1)*sizeof(char));
            strcpy(cityList[i].name, tName);
            i++;
        }
    }
    
    *actualSize = i;
    
    //Shrinking array to fit
    cityList = xRealloc(cityList, i*(sizeof(CITY)));
    
    fclose(fpIn);

    return cityList;
}

void insertionSort(CITY* cityList, int actualSize) {
    /* function that accepts the city array and the size of the array in order to sort it. */
    int i, j;
    CITY temp;
    char tName[100];
    
    for(i = 0; i < actualSize; i++) {
        strcpy(tName, (cityList[i]).name);
        temp = cityList[i];
        for(j = i - 1; j >= 0; j--) {
            if(strcmp(cityList[j].name, tName) <= 0) break;
            cityList[j + 1] = cityList[j];
        }
        cityList[j + 1] = temp;
    }

    return;
}

void calcAvgs(CITY* cityList, int actualSize, char outputName[]) {
    /* function that accepts the city array, the size of the city array, and the
	user's input for the output file name in order to calculate the average high
	temperatures and output them to the file. */

    FILE* fpOut = fopen(outputName, "w");
    int i, j, sum = 0, count = 0, spot = 0;
    double avg;
    fprintf(fpOut, "\nEach city's average high temperature:");
    
    for(i = 0; i < actualSize; i = spot) {
        for(j = 0; j < actualSize; j++) {
            if(strcmp(cityList[i].name, cityList[j].name) == 0) {
                sum += cityList[j].temperature;
                count++;
                spot++;
            }
        }
        avg = (double)sum/count;
        fprintf(fpOut, "%s %.2f", cityList[i].name, avg);
        sum = 0;
        count = 0;
    }
    
    fclose(fpOut);
    printf("Write to file %s successful.", outputName);
    
    return;
}

void printList(CITY* cityList, int actualSize) {
	/* function that accepts the city array and the size of the array to 
	print it. */
	CITY* pCity;
	CITY* pLast;

    printf("Full list of states, cities and temperatures:");
	for(pCity = cityList, pLast = cityList + actualSize - 1; pCity <= pLast; pCity++) {
		printf("%s %d", pCity->name, pCity->temperature);
	}
}

void destroyList(CITY* cityList, int actualSize) {
	/* function that accepts the city array and it's size in order to free
	memory allocated earlier in the program. */
	CITY* pCity;
	CITY* pLast;

    for(pCity = cityList, pLast = cityList + actualSize - 1; pCity <= pLast; pCity++) {
        free(pCity->name);
    }
    free(cityList);
}

/*
SCREEN OUTPUT: 

Enter name of input file: inputFile.txt
Full list of states, cities and temperatures:
Arizona,Flagstaff 81
Arizona,Flagstaff 84
Arizona,Phoenix 109
Arizona,Phoenix 107
Arizona,Tucson 107
Arizona,Tucson 99
Arizona,Tucson 103
California,Los Angeles 78
California,Los Angeles 81
California,Los Angeles 82
California,San Francisco 64
California,San Francisco 68
California,Yreka 100
Oregon,Portland 82
Oregon,Portland 79
Oregon,Salem 90
Oregon,Salem 83
Oregon,Salem 85
Pennsylvania,Philadelphia 91
Pennsylvania,Philadelphia 86
Pennsylvania,Pittsburgh 89

Enter name for output file (use .txt): output.txt
Write to file output.txt successful.
No Memory Leak
Press any key to continue . . .

FILE OUTPUT:

Each city's average high temperature:
Arizona,Flagstaff 82.50
Arizona,Phoenix 108.00
Arizona,Tucson 103.00
California,Los Angeles 80.33
California,San Francisco 66.00
California,Yreka 100.00
Oregon,Portland 80.50
Oregon,Salem 86.00
Pennsylvania,Philadelphia 88.50
Pennsylvania,Pittsburgh 89.00
*/