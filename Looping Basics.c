/*Write a complete C program that creates the following pattern, given
the height (how many rows of '*'s and the width (number of characters
across i.e. number of '=' across.
======
*    *
*    *
*    *
======

Write your name as a comment and print your name to the output as part
of the code. Copy and paste the output as a comment on the bottom of
the code using a test input of 5 for the height and 7 for the width.
*/

#include <stdio.h>

int main (void)
{
    int height;
    int width;
    int x;
    int y;

    printf("Enter height: ");
    scanf("%d", &height);
    printf("\nEnter width: ");
    scanf("%d", &width);

    for(x = 0; x < width; x++)
    {
        printf("=");
    }

    printf("\n");

    for(y = 0; y < height; y++)
    {
        printf("*");

        for(x = 0; x < width-2; x++)
        {
            printf(" ");
        }

        printf("*\n");

    }

    for(x = 0; x < width; x++)
    {
        printf("=");
    }

    printf("\n");

    return 0;
}
