/*Using the products.txt file, write a function to input the
 info into parallel arrays.

 � Add to this program a selection sort to output the products
   from the lowest to the highest. Output the result from main().

 � Add a function that will output the quantity on hand and the
   price when given the product ID code.

 � Add a function to output the product IDs and current number
   in stock for all products needing to be reordered.
 */

#include <stdio.h>
#include <stdlib.h>

#define MAX 50

int getdata(FILE*, int[], double[], int[], int[]);
void output(int[], double[], int[], int[], int);
void find(int id[], int qty[], int num);

 int main (void)
 {
    int id[MAX];
    double price[MAX];
    int qty[MAX];
    int rop[MAX];
    int num;

    FILE* fpIn;
    fpIn = fopen("products.txt", "r");

    if(!(fpIn = fopen("products.txt", "r")))
    {
        printf("\nFILE DOES NOT EXIST");
        exit(100);
    }

    num = getdata(fpIn, id, price, qty, rop);
    output(id, price, qty, rop, num);
    find(id, qty, num);

    fclose(fpIn);

    return 0;
 }

int getdata(FILE* fpIn, int id[], double price[], int qty[], int rop[])
{
    /*Pre: Pointer to the file for input that is already open, arrays for
    id, price, quantity, and reorder point of products.*/
    //Post: Number of products
    int count = 0;

    while(count < MAX && fscanf(fpIn, "%d %lf %d %d",
                            &id[count], &price[count], &qty[count], &rop[count])!= EOF)
                            count++;

    return count;
}

void output (int id[], double price[], int qty[], int rop[], int num)
{
    int count;

    for(count=0; count<num; count++)
    printf("%4d %9.2lf %4d %4d %4d\n", id[count], price[count], qty[count], rop[count], num);

    return;
}

void find(int id[], int qty[], int num)
{
    int target;
    int walk;

    printf("\n\nEnter product ID number: ");
    scanf("%d", &target);

    for(walk=0; walk<num; walk++)
    {
        if(id[walk] == target)
        {
            printf("\nFound quantity %d\n", qty[walk]);
        }
    }

    return;
}

