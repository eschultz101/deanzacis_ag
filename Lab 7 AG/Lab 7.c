/* Eric Schultz
Lab 7
eschultz@pacbell.net
*/

#include <stdio.h>
#include <stdlib.h>

#define MAX 40

void getdata();
void output(int quizScore1[], int quizScore2[], int quizScore3[],
            int quizScore4[], int quizScore5[], int high[], int low[],
            float avg1, float avg2, float avg3, float avg4, float avg5,
            int s_count, int sum1, int sum2, int sum3, int sum4, int sum5,
            int studentid[]);
void highcalc(int quizScore1[], int quizScore2[], int quizScore3[],
             int quizScore4[], int quizScore5[], int s_count, int high[]);
void lowcalc(int quizScore1[], int quizScore2[], int quizScore3[],
            int quizScore4[], int quizScore5[], int s_count, int low[]);
void averagecalc(int* sum1, int* sum2, int* sum3, int* sum4, int* sum5, int s_count,
                 float* avg1, float* avg2, float* avg3, float* avg4, float* avg5, int studentid[],
                 int quizScore1[], int quizScore2[],int quizScore3[], int quizscore4[], int quizscore5[]);

int main (void)
{
    //Pre: none
    //Post: getdata function

    //function definition
    getdata();

    return 0;
}

void getdata ()
{
    //Pre: none
    //Post: Student ID numbers, quiz scores, total number of students.

    //Opening file that has input
    FILE* fpIn;
    fpIn = fopen("lab7input.txt", "r");

    if (!(fpIn = fopen("lab7input.txt", "r")))
       {
           printf("No such file");
           exit(100);
       }

    int s_count = 0;
    int sum1 = 0;
    int sum2 = 0;
    int sum3 = 0;
    int sum4 = 0;
    int sum5 = 0;
    int quizScore1[MAX];
    int quizScore2[MAX];
    int quizScore3[MAX];
    int quizScore4[MAX];
    int quizScore5[MAX];
    int studentid[MAX];
    float avg1 = 0;
    float avg2 = 0;
    float avg3 = 0;
    float avg4 = 0;
    float avg5 = 0;
    int low[5] = {100, 100, 100, 100, 100};
    int high[5] = {0};
    int i;

     while((s_count < MAX) && (fscanf(fpIn, "%d %d %d %d %d %d", &studentid[s_count], &quizScore1[s_count],
            &quizScore2[s_count], &quizScore3[s_count], &quizScore4[s_count], &quizScore5[s_count])!= EOF))
            s_count++;

    //function definition
    output(quizScore1,quizScore2,quizScore3,quizScore4,quizScore5,high,low,avg1,avg2,avg3,avg4,avg5,
           s_count,sum1,sum2,sum3,sum4,sum5,studentid);

    //Closing file that has input
    fclose(fpIn);

    return;
}

void output (int quizScore1[], int quizScore2[], int quizScore3[],
             int quizScore4[], int quizScore5[], int high[], int low[],
             float avg1, float avg2, float avg3, float avg4, float avg5,
             int s_count, int sum1, int sum2, int sum3,
             int sum4, int sum5, int studentid[])
{
    /*Pre: Quiz scores, the highest, lowest, and average scores for each quiz,
    sums of the five quizes by student for use in averagecalc, and the student's IDs.*/
    //Post: none
    int i;

    //Function definitions
    highcalc(quizScore1,quizScore2,quizScore3,quizScore4,quizScore5,s_count,high);
    lowcalc(quizScore1,quizScore2,quizScore3,quizScore4,quizScore5,s_count,low);
    averagecalc(&sum1,&sum2,&sum3,&sum4,&sum5,s_count,&avg1,&avg2,&avg3,&avg4,&avg5,
                studentid,quizScore1,quizScore2,quizScore3,quizScore4,quizScore5);

    //Opening output file
    FILE* fpOut;
    if (!(fpOut = fopen("lab7output.txt", "w")))
       {
           printf("No such file");
           exit(100);
       }

    //Print the header
    fprintf(fpOut, "\n    Student\tQuiz 1\tQuiz 2\tQuiz 3\tQuiz 4\tQuiz 5");

    //Print quiz scores for number of inputted students.
    for(i = 0; i < s_count; i++)
    {
    fprintf(fpOut, "\n%10d\t%5d\t%5d\t%5d\t%5d\t%5d",
            studentid[i], quizScore1[i], quizScore2[i], quizScore3[i], quizScore4[i], quizScore5[i]);
    }
    //Print highest, lowest, and average score for each student's quizzes.
    fprintf(fpOut, "\n\nHigh Score\t%5d\t%5d\t%5d\t%5d\t%5d", high[0], high[1], high[2], high[3], high[4]);
    fprintf(fpOut, "\nLow Score\t%5d\t%5d\t%5d\t%5d\t%5d", low[0], low[1], low[2], low[3], low[4]);
    fprintf(fpOut, "\nAverage Score\t%5.1f\t%5.1f\t%5.1f\t%5.1f\t%5.1f", avg1, avg2, avg3, avg4, avg5);
    fprintf(fpOut, "\n\nEric Schultz\nLab 7\neschultz@pacbell.net");

    //Closing output file
    fclose(fpOut);

    return;
}

void highcalc(int quizScore1[], int quizScore2[], int quizScore3[], int quizScore4[], int quizScore5[], int s_count, int high[])
{
    //Pre: Quiz scores, number of students, and highest score array.
    //Post: Highest scores for each quiz.
    int j;

    //Loop for finding and assigning the highest score for each quiz.
    for (j = 0; j<s_count; j++)
        {

            if (quizScore1[j]>high[0])
                high[0] = quizScore1[j];

            if (quizScore2[j]>high[1])
                high[1] = quizScore2[j];

            if (quizScore3[j]>high[2])
                high[2] = quizScore3[j];

            if (quizScore4[j]>high[3])
                high[3] = quizScore4[j];

            if (quizScore5[j]>high[4])
                high[4] = quizScore5[j];
        }
}

void lowcalc(int quizScore1[], int quizScore2[], int quizScore3[],
             int quizScore4[], int quizScore5[], int s_count, int low[])
{
    //Pre: Quiz scores, number of students, lowest score array.
    //Post: Lowest scores for each quiz.
    int i;

    //Loop for finding and assigning the lowest score for each quiz.
     for (i=1; i < s_count; i++)
        {

            if(low[0]>quizScore1[i])
            low[0] = quizScore1[i];

            if(low[1]>quizScore2[i])
            low[1] = quizScore2[i];

            if(low[2]>quizScore3[i])
            low[2] = quizScore3[i];

            if(low[3]>quizScore4[i])
            low[3] = quizScore4[i];

            if(low[4]>quizScore5[i])
            low[4] = quizScore5[i];
    }
}

void averagecalc(int* sum1, int* sum2, int* sum3, int* sum4, int* sum5, int s_count,
                 float* avg1, float* avg2, float* avg3, float* avg4, float* avg5,
                 int studentid[], int quizScore1[],int quizScore2[],
                 int quizScore3[], int quizScore4[], int quizScore5[])
{
    /*Pre: Sum of each quiz for all students, the number of students,
    the averages themselves, student IDs and quiz scores.*/
    //Post: Average values for each quiz.
    int i;

    for (i=0; i < s_count; i++)
    {
        *sum1 += quizScore1[i];
        *sum2 += quizScore2[i];
        *sum3 += quizScore3[i];
        *sum4 += quizScore4[i];
        *sum5 += quizScore5[i];
    }

        *avg1 = ((float)*sum1)/((float)s_count);
        *avg2 = ((float)*sum2)/((float)s_count);
        *avg3 = ((float)*sum3)/((float)s_count);
        *avg4 = ((float)*sum4)/((float)s_count);
        *avg5 = ((float)*sum5)/((float)s_count);

        /*When I tried to use arrays for int sum and int avg the numbers
        were always strange, but I seem to get correct values this way.*/

    return;
}
