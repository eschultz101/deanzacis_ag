#include <stdio.h>
#include <stdlib.h>

double highest(double [], int);

 int main (void)
 {
     int pin[44]; //parallel arrays (score and pin).
     double score[44]; int i; double avg;
     double sum = 0; int count = 0;
     double low; double high;
     FILE* fpIn;

     //Open file checking for existence
     if (!(fpIn=fopen("test.txt", "r")))
       {
           printf("No such file"); exit(100);
       }
     //Input student pin numbers and scores from file
     while (fscanf(fpIn, "%d", &pin[count])!= EOF)
     {
     fscanf(fpIn, "%lf", &score[count]);
     sum += score[count];
     count++;
     }
     //Output sum and average for all scores
 printf("\nThe sum is: %.2f", sum);
 avg = sum / count;
    printf("\nThe average is: %.2f", avg);

    //find lowest score

    low = score[0];
    for (i=1; i < count; i++)
    {
        if(low>score[i])
        low = score[i];
    }

    high = highest(score, count);

    printf("\nThe lowest score is: %.0lf", low);
    printf("\nThe highest score is: %.0lf", high);
     //Output heading for table
 printf("\n\n      PIN #    SCORE    DEVIATION");
 //Output pin number, score and deviation from the average for each student
 for(i = 0; i < count; i++)
 printf("\n%10d%10.2f%10.2f", pin[i], score[i], score[i] - avg);

 printf("\n");

 return 0;
 }

 double highest(double arr[], int num)
 {
    double high;
    int i;

    high = arr[0];
    for (i = 1; i < num; i++)
    {
        if(high<arr[i])
        high = arr[i];
    }
     return high;
 }

