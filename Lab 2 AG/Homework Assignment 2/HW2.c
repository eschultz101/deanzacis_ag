/*********************************************************************************
** CIS 26B															  Spring, 2013
** Advanced C
******************
**
** Homework 2: STOCKS
**        A Circularly Doubly Linked List of Stacks
**
**********************************************************************************

   Stocks...

   Save the output as a comment at the end of the program!
**********************************************************
**
**  Written By: Eric Schultz         
**
**  Date: 4/25/13         
*********************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <ctype.h>

#include "wrapper.c"

#ifdef _MSC_VER
#include <crtdbg.h>
#endif

typedef struct stackNode {
	double quote[10];
	int size;
}STACK;

typedef struct doubleNode {
	char symbol[10];
	STACK* sPtr;
	struct doubleNode* forw;
	struct doubleNode* back;
}DOUBLE;

typedef struct doubleList {
	DOUBLE* first;
	int count;
	DOUBLE* last;
}LIST;

LIST* getData();
void printList(LIST* list);
void printQuotes(LIST* list);
void printSublist(LIST* list);
void destroyList(LIST* list);

int main( void )
{
	LIST* list;

	list = getData();
	printList(list);
	printQuotes(list);
	printSublist(list);
	destroyList(list);

    #ifdef _MSC_VER
    printf( _CrtDumpMemoryLeaks() ? "\nMemory Leak\n\n" : "\nNo Memory Leak\n\n");
    #endif
	
	system("pause");

    return 0;
}

LIST* getData() {
	/* Function that accepts no parameters - it allocates memory for all necessary
	nodes and structures and fills them with data from the file.  All data in the 
	file is assumed to be pre-validated.  Returns doubly circularly linked list */

	FILE* fpIn;
	LIST* list;
	char inputName[25];
	char tempSymbol[10];
	double tempQuote;
	int flag = 0;
	DOUBLE* newNode;
	STACK* newStack;
	DOUBLE* walker;
	DOUBLE* sentinel = (DOUBLE*)xMalloc(sizeof(DOUBLE));

	
	printf("Enter name of input file: ");
	scanf("%s", &inputName);

	fpIn = xFopen(inputName, "r");

	list = (LIST*)xMalloc(sizeof(LIST));
	list->count = 0;

	while(fscanf(fpIn, "%s %lf", &tempSymbol, &tempQuote) != EOF) {
		//If this is first line in the file:
		if(list->count == 0) {
			newNode = (DOUBLE*)xMalloc(sizeof(DOUBLE));
			list->first = newNode;
			list->last = newNode;
			newStack = (STACK*)xMalloc(sizeof(STACK));

			//initializing new node's fields
			newNode->back = NULL;
			newNode->forw = NULL;
			newNode->sPtr = newStack;
			newNode->sPtr->size = 0;

			//assigning values
			strcpy(newNode->symbol, tempSymbol);
			newNode->sPtr->quote[newNode->sPtr->size++] = tempQuote;
		}
		//If not first line in the file:
		else if(list->count > 0) {
			walker = list->first;
			//Check every other symbol in the existing list
			while(walker) {
				if(strcmp(walker->symbol, tempSymbol) == 0) {
					walker->sPtr->quote[walker->sPtr->size++] = tempQuote;
					flag = 1;
				}
				if(walker->forw) {
					walker = walker->forw;
				} else {
					walker = NULL;
				}
			}
			if(flag == 0) {
				newNode = (DOUBLE*)xMalloc(sizeof(DOUBLE));
				newStack = (STACK*)xMalloc(sizeof(STACK));

				//initializing new node's fields
				newNode->back = NULL;
				newNode->forw = NULL;
				newNode->sPtr = newStack;
				newNode->sPtr->size = 0;

				//assigning values
				strcpy(newNode->symbol, tempSymbol);
				newNode->sPtr->quote[newNode->sPtr->size++] = tempQuote;
				list->last->forw = newNode;
				newNode->back = list->last;

				list->last = newNode;
			}//if(flag == 0)
		}//if(count > 0)
		(list->count)++;
	}//while

	//create sentinel node
	strcpy(sentinel->symbol, "END");
	sentinel->sPtr = NULL;

	//attach sentinel to "end" of list, make list cirularly linked
	sentinel->back = list->last;
	list->last->forw = sentinel;
	sentinel->forw = list->first;
	list->first->back = sentinel;
	list->last = sentinel;

	return list;
}//getData()

void printList(LIST* list) {
	/* Function that accepts the list and prints out its contents. Returns nothing */

	DOUBLE* walker = list->first;
	double temp;
	int count;

	printf("SYMBOLS\tQUOTES\n");

	while(strcmp(walker->symbol, "END") != 0) {
		printf("%7s", walker->symbol);
		count = walker->sPtr->size;
		while(count > 0) {
			temp = walker->sPtr->quote[--count];
			printf(" %5.2lf", temp);
		}
		printf("\n");
		walker = walker->forw;
	}

	printf("\nNumber of quotes in list: %d", list->count);

	return;
}

void printQuotes(LIST* list) {
	/* Function that accepts the list of symbols and prints out a specified
	symbol along with a specified number of recent quotes. Returns nothing */

	int numQuotes, i, count;
	char tempSymbol[10] = "default";
	DOUBLE* walker;

	while(strcmp(tempSymbol, "END") != 0) {

		printf("\n\nEnter a symbol to display quotes for (END to quit): ");
		scanf("%s", &tempSymbol);
		for(i = 0; i < strlen(tempSymbol); i++) {
			tempSymbol[i] = toupper(tempSymbol[i]);
		}
		if(strcmp(tempSymbol, "END") == 0) {
			return;
		}
		printf("\nEnter number of recent quotes to print: ");
		scanf("%d", &numQuotes);

		walker = list->first;

		while(strcmp(walker->symbol, "END") != 0) {
			if(strcmp(walker->symbol, tempSymbol) == 0) {
				printf("\n%d most recent quote(s) for %s: ", numQuotes, tempSymbol);
				count = walker->sPtr->size;
				while(numQuotes > 0 && count > 0) {
					printf("%.2lf ", walker->sPtr->quote[--count]);
					--numQuotes;
				}
			}
			walker = walker->forw;
		}
	}
	return;
}

void printSublist(LIST* list) {
	/* Function that accepts the list of symbols and prints out, in ascending and
	descending order, those symbols specified and all symbols in between along 
	with their most recent quotes.  Returns nothing */

	char startSymbol[10] = "default";
	char endSymbol[10];
	DOUBLE* startNode = NULL;
	DOUBLE* endNode = NULL;
	DOUBLE* walker;
	int count, i, flag1 = 0, flag2 = 0;

	while(strcmp(startSymbol, "END") != 0) {

		printf("\nEnter start point symbol (END to quit): ");
		scanf("%s", startSymbol);
		for(i = 0; i < strlen(startSymbol); i++) {
			startSymbol[i] = toupper(startSymbol[i]);
		}
		if(strcmp(startSymbol, "END") == 0) {
			return;
		}

		printf("Enter end point symbol: ");
		scanf("%s", endSymbol);
		for(i = 0; i < strlen(endSymbol); i++) {
			endSymbol[i] = toupper(endSymbol[i]);
		}

		//finding specified start and end nodes
		walker = list->first;
		while(strcmp(walker->symbol, "END") != 0) {
			if(strcmp(walker->symbol, startSymbol) == 0) {
				startNode = walker;
				flag1 = 1;
			}
			if(strcmp(walker->symbol, endSymbol) == 0) {
				endNode = walker;
				flag2 = 1;
			}
			walker = walker->forw;
		}
		if(flag1 == 0 || flag2 == 0) {
			printf("\nStart or end symbol not found");
			return;
		}

		//descending order
		walker = startNode;
		printf("\nDescending order:\n");

		while(walker->back != endNode) {
			if(strcmp(walker->symbol, "END") != 0) {
				count = walker->sPtr->size;
				printf("%s: %.2lf\n", walker->symbol, walker->sPtr->quote[--count]);
			}
			walker = walker->forw;
		}

		//ascending order
		walker = endNode;
		printf("\nAscending order:\n");

		while(walker->forw != startNode) {
			if(strcmp(walker->symbol, "END") != 0) {
				count = walker->sPtr->size;
				printf("%s: %.2lf\n", walker->symbol, walker->sPtr->quote[--count]);
			}
			walker = walker->back;
		}

	}
	return;
}

void destroyList(LIST* list) {
	/* Function that accepts the list of symbols and frees all memory
	allocated when the list was created.  Returns nothing */
	DOUBLE* walker;
	walker = list->first->forw;

	while(strcmp(walker->symbol, "END") != 0) {
		free(walker->back->sPtr);
		free(walker->back);
		walker = walker->forw;
	}

	free(list->last->back->sPtr);
	free(list->last->back);
	free(list->last);
	free(list);

	return;
}

/*  OUTPUT

Enter name of input file: in.txt
SYMBOLS QUOTES
    BAC 11.97 12.44
   MSFT 32.14 30.56 31.94
   INTC 24.11 23.38
      S  8.09  7.14
   ZNGA  3.11  3.13
   QCOM 62.44
    BSX  8.60  7.54
      F 12.44 13.47
     GE 22.00 21.95
   CSCO 21.37 20.64

Number of quotes in list: 20

Enter a symbol to display quotes for (END to quit): MSFT

Enter number of recent quotes to print: 1

1 most recent quote(s) for MSFT: 32.14

Enter a symbol to display quotes for (END to quit): MSFT

Enter number of recent quotes to print: 3

3 most recent quote(s) for MSFT: 32.14 30.56 31.94

Enter a symbol to display quotes for (END to quit): QCOM

Enter number of recent quotes to print: 2

2 most recent quote(s) for QCOM: 62.44

Enter a symbol to display quotes for (END to quit): end

Enter start point symbol (END to quit): BAC
Enter end point symbol: s

Descending order:
BAC: 11.97
MSFT: 32.14
INTC: 24.11
S: 8.09

Ascending order:
S: 8.09
INTC: 24.11
MSFT: 32.14
BAC: 11.97

Enter start point symbol (END to quit): F
Enter end point symbol: s

Descending order:
F: 12.44
GE: 22.00
CSCO: 21.37
BAC: 11.97
MSFT: 32.14
INTC: 24.11
S: 8.09

Ascending order:
S: 8.09
INTC: 24.11
MSFT: 32.14
BAC: 11.97
CSCO: 21.37
GE: 22.00
F: 12.44

Enter start point symbol (END to quit): end

No Memory Leak

Press any key to continue . . .

*/