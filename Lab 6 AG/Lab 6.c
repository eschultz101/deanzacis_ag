
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

void getdata(float *pricepb, float *numberBonds, float *iRate, int *years);
void calc(int years, float* payment, float *value, float iRate,
           float* interest, float pricepb, float* retiredAmt, FILE* fpOut,
           float numberBonds);

int myround(float);

int main (void)
{
    //Pre: none
    //Post: none

    float pricepb;
    float numberBonds;
    float iRate;
    int years;
    float value;
    float payment;
    float retiredAmt;
    float interest;

    //Opening file to print table on
    FILE* fpOut;
    fpOut = fopen("Lab 6 output.txt", "w");

    printf("Eric Schultz\nLab 6\neschultz@pacbell.net\n\n");

    getdata(&pricepb, &numberBonds, &iRate, &years);
    calc(years,&payment,&value,iRate,&interest,pricepb,&retiredAmt,fpOut, numberBonds);

    fclose(fpOut);

    return 0;
}

void getdata(float *pricepb, float *numberBonds, float *iRate, int *years)
{
    printf("Enter price per bond: ");
    scanf("%f", pricepb);
    printf("\nEnter initial number of bonds: ");
    scanf("%f", numberBonds);
    printf("\nEnter interest rate (percent): ");
    scanf("%f", iRate);
    printf("\nEnter number of years over which debt is payed: ");
    scanf("%d", years);

    return;
}

void calc (int years, float* payment, float* value, float iRate,
           float* interest, float pricepb, float* retiredAmt, FILE* fpOut, float numberBonds)
{
    int yearsleft;
    float temp;
    int temp2;

    iRate/=100;
    *value = pricepb*numberBonds;

    fprintf(fpOut, "\nYEAR     VALUE           INTEREST         PAYMENT       AMT RETIRED");

    for(yearsleft = years; yearsleft>0; yearsleft--)
    {

        *payment = (*value*iRate)/(1-(1/(pow(1 + iRate, yearsleft))));
        *interest = iRate**value;

        temp = ((*payment - (*interest))/pricepb);

        temp2 = myround(temp);

        *retiredAmt = temp2*pricepb;

        fprintf(fpOut, "\n%2d      $%9.2f      $%9.2f      $%9.2f      $%9.2f",
                        yearsleft, *value, *interest, *payment, *retiredAmt);

        *value = *value - *retiredAmt;
    }
    return;
}


int myround(float num)
{
    //Pre: temporary value for part of expression that needs to be rounded
    //Post: rounded up or down number
    int ans;
    int iNum;
    int dec;
    int rnum;

    //rounding function!
    iNum = (int)num;
    dec = (int)((num-(float)iNum)*10);

    if(dec>=5)
        rnum = ceil(num);
    else
        rnum = floor(num);

    return(rnum);
}


