/*********************************************************

Homework 5
Eric Schultz
6/13/13

Program which generates a cross reference listing of the 
identifiers in an input C program.

*********************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "wrapper.h"

typedef struct treenode {

	char* identifier;
	int lineNums[20];
	int queueNum;
	struct treenode *left, *right;

}NODE;

int main (int argc, char* argv[]) {

	int getData(char *inputFile, NODE** tree);
	void printTree(NODE* tree, FILE*);

	char *inputFile, *outputFile;
	FILE* fpOut;
	NODE* tree = NULL;
	int lineCount;
	struct tm *timeptr;
    time_t timeval;
    char *chtime;

	inputFile = argv[1];
	outputFile = argv[2];

	lineCount = getData(inputFile, &tree);
	fpOut = xFopen(outputFile, "w");
	printTree(tree, fpOut);

	fprintf(fpOut, "\n\nNumber of lines in program: %d\n", lineCount);

	time(&timeval);
    timeptr = localtime(&timeval);
	fprintf(fpOut, "Identifier listing created %s \n\n", 
									asctime(timeptr));

	fclose(fpOut);

	return 0;
}

int getData(char *inputFile, NODE** tree) {
	/*Function that accepts the user-entered name for the 
	input file and the address of the first tree node.  
	The tree is created and filled and the total number
	of lines is returned. */

	void insert(char* token, NODE** tree, int lineCount);
	void processLine(char* line, char* tokens[50]);

	FILE* fpIn;
	int i = 0, j = 0;
	int lineCount = 0;
	char buffer[300];
	char* tokens[50] = {{""}};

	fpIn = xFopen(inputFile, "r");

	while(fgets(buffer, 100, fpIn)) {
		processLine(buffer, tokens);
		while(tokens[i]) {
			insert(tokens[i], tree, lineCount);
			++i;
		}
		lineCount++;
		i = 0;
	}
	fclose(fpIn);
	return lineCount;
}

void processLine(char* line, char* tokens[50]) {
	/*Function that accepts one line of the .c file entered and a two 
	dimensional array that will be filled with each identifier, after
	processing the line.  It then removes commented and quoted 
	identifiers.  Returns nothing */
	int i = 1, j;

	if(tokens[0] = strtok(line, "\040\t\n(){}\',;[]0123456789*=")) {

		while(tokens[i] = strtok(NULL, "\040\t\n(){}\',;[]0123456789*=")) {
			 ++i;
		} 
		//Removing tokens that are commented (using //)
		i = 0;
		while(tokens[i]) {
			if(tokens[i][0] == '/' && tokens[i][1] == '/') {
				j = i;
				while(tokens[j]) {
					tokens[j] = NULL;
					j++;
				}
			}
			i++;
		}
		//Removing tokens inside double quotes
		i = 0;
		while(tokens[i]) {
			if(tokens[i][0] == '\"') {
				j = i;
				while(tokens[j]) {
					if(tokens[j][strlen(tokens[j])] != '\"') {
						tokens[j] = NULL;
						j++;
					}
				}
			}
			i++;
		}
		//Removing tokens inside single quotes
		i = 0;
		while(tokens[i]) {
			if(tokens[i][0] == '\'') {
				j = i;
				while(tokens[j]) {
					if(tokens[j][strlen(tokens[j])] != '\'') {
						tokens[j] = NULL;
						j++;
					}
				}
			}
			i++;
		}

	//If no tokens found, return nothing
	} else {
		return;
	}
	return;
}

void insert(char* token, NODE** tree, int lineCount) {
	/*Function that accepts a single token, the first tree node, 
	and the current line.  The function is called recursively to
	fill the entire tree with identifiers, and if the identifier 
	is already found then the line number is added to the tree 
	node's queue.  Returns nothing */
	int compare;
	int i = 0;

	if(!(*tree)) {
		*tree = (NODE*)xMalloc(sizeof(NODE));
		(*tree)->queueNum = 1;
		(*tree)->identifier = (char*)xMalloc(strlen(token + 1));
		(*tree)->left = (*tree)->right = NULL;
		strcpy((*tree)->identifier, token);
		(*tree)->lineNums[0] = lineCount;
		return;
	}
	compare = strcmp(token, (*tree)->identifier);
	if(compare < 0) insert(token, &(*tree)->left, lineCount);
	else if(compare > 0) insert(token, &(*tree)->right, lineCount);
	else (*tree)->lineNums[((*tree)->queueNum)++] = lineCount;
	return;
}

void printTree(NODE* tree, FILE* fpOut) {
	/* Function that accepts the parent tree node, and 
	the output file name. Prints the tree in its entirety 
	including the line numbers each identifier is found 
	at.  Returns nothing */
	int i = 0;

	if(!tree) return;

	printTree(tree->left, fpOut);
	fprintf(fpOut, "\n%15s", tree->identifier);
	while(i < tree->queueNum) {
		fprintf(fpOut, "%10d", tree->lineNums[i]);
		i++;
	}
	printTree(tree->right, fpOut);

	return;
}

/*********************************
OUTPUT
C:\Users\Eric\My Documents\visual studio 2010\Projects\Homework Assignment 5\Debug>start "" "Homework Assignment 5.exe"
test.c output

           NODE         2         5
           NULL         5
           char         2         4
        getData         2        13
      inputFile         2         4        10        13
            int         0         2         6
      lineCount         6        13
           main         0
     outputFile         4        11
         return        15
         strcpy        10        11
           tree         2         5        13
           void         0

Number of lines in program: 17
Identifier listing created Thu Jun 13 23:34:21 2013