/*********************************************************************************
** CIS 26B															  Spring, 2013
** Advanced C
******************
**
** Homework 3: Insert/Delete/Display database records
**        in a financial aid database // BINARY FILES
**
**********************************************************************************
   Save the output as a comment at the end of the program!
**********************************************************
**
**  Written By: Eric Schultz
**
**  Date: 5/16/13
***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include "wrapper.h"

#define BUCKETSIZE 4
#define TABLESIZE 11
#define INSERT 1
#define SEARCH 0
#define DELETE -1

typedef struct {
	char firstname[20];
	char lastname[20];
	double amount;
	int student_id;
} RECORD;

void search_or_insert(FILE* fp, int insertflag);

int main( int argc, char* argv[] )
{
	 
	FILE* fp;
	FILE* create_hash_file(char* filename);

	char temp[20];
	char choice;
	char* filename = argv[1];

	void search_or_insert(FILE* fp, int insertflag);

	fp = create_hash_file(filename); //change to argv[1] later
	printf("INSERT RECORDS: \n");
	search_or_insert(fp, INSERT);

	printf("Enter search, delete, add, or quit: ");
	scanf("%s", &temp);
	fflush(stdin);

	choice = tolower(temp[0]);

	while(choice != 'q') {

		if(choice == 'a') {
			printf("INSERT RECORDS: \n");
			search_or_insert(fp, INSERT);
		}
		else if(choice == 'd') { 
			printf("DELETE RECORDS: \n");
			search_or_insert(fp, DELETE);
		}
		else if(choice == 's') { 
			printf("SEARCH FOR RECORDS: \n");
			search_or_insert(fp, SEARCH);
		}
		else {
			printf("Invalid choice.\n");
		}

		printf("Enter search, delete, add, or quit: ");
		scanf("%s", &temp);
		fflush(stdin);
		choice = tolower(temp[0]);
	}

	fclose(fp);
    return 0;
}

FILE* create_hash_file(char* filename) {
	/*create_hash_file: function that opens a binary file and prepares to write
	to it.  Returns a pointer to the file. */

	FILE* fp;

	RECORD hashtable[TABLESIZE][BUCKETSIZE] = {{"","", 0, 0}};

	fp = xFopen(filename, "w+b");

	if(fwrite(&hashtable[0][0], sizeof(RECORD), TABLESIZE * BUCKETSIZE, fp) < TABLESIZE) {

		printf("Hash table could not be created.\n");
		exit(1);

	}

	rewind(fp);

	return fp;
}

int hasher(char* key) {
	/* hasher: function that accepts the key (the first name in this case)
	and returns the index number generated using the first name. */

	long hash;
	char* walker;

	hash =  0;
	for(walker = key; *walker != '\0'; walker++) {

		hash += (*walker) * (*walker) * (*walker);
	}

	hash = hash % TABLESIZE;

	return hash;
}

void search_or_insert(FILE* fp, int insertflag) {
	/* search_or_insert: function that accepts the hashtable file and a flag
	which indicates what action is to be preformed, either add, search, or 
	delete.  Returns nothing. */

	char line[100], *firstname, *walker;
	long hash;
	int hasher(char* key);
	void insert_record(char* key, char* line, long hash, FILE* fp);
	void search_record(char* key, long hash, FILE* fp);
	void delete_record(char* key, long hash, FILE* fp);

	while(printf("Enter student info (or type quit to go to menu): "), gets(line), strcmp(line, "quit") != 0) { 
		
		firstname = strtok(line, "\040\t");
		for(walker = firstname; *walker != '\0'; walker++) {
			*walker = toupper(*walker);
		}

		hash = hasher(firstname);
		if(insertflag == 1) {
			insert_record(firstname, line, hash, fp);
		}
		else if(insertflag == 0) {
			search_record(firstname, hash, fp);
		}
		else if(insertflag == -1) {
			delete_record(firstname, hash, fp);
		}
	}

	return;
}

void insert_record(char* key, char* line, long hash, FILE* fp) {
	/*insert_record: function that is called by search_or_insert to 
	insert a new node into the hash table.  Accepts the key (first name),
	the line entered by the user, the hashed index, and the hash table file.  
	Returns nothing */

	RECORD detect, temp;
	int i;
	char *lname, *sAmt, *sStuID, *endPtr, *walker, *pos;
	double amt;
	int studID;

	lname = strtok(NULL, "\040\t"); //Need all lower or uppercase name/last name.
	for(walker = lname; *walker != '\0'; walker++) {
		*walker = toupper(*walker);
	}

	sAmt = strtok(NULL, "\040\t");
	pos = strrchr(sAmt, '.');  
	if(strlen(pos) != 3) {
		printf("Must enter amount with two decimal places.\nRecord not entered.\n\n");
		return;
	}
	if(strlen(sAmt) > 8 || strlen(sAmt) < 5) {
		printf("Must enter amount between 10.00 and 99,999.99.\nRecord not entered.\n\n");
		return;
	}

	sStuID = strtok(NULL, "\040\t");
	if(strlen(sStuID) != 4) {
		printf("Student ID must be 4 integers in length.\nRecord not entered.\n\n");
		return;
	}

	amt = strtod(sAmt, &endPtr);
	studID = strtol(sStuID, &endPtr, 10);

	strcpy(temp.firstname, key);
	strcpy(temp.lastname, lname);

	temp.amount = amt;
	temp.student_id = studID;

	if(fseek(fp, hash * BUCKETSIZE * sizeof(RECORD), SEEK_SET) != 0) {
		printf("Fatal seek error!\n");
		exit(2);
	}

	for(i = 0; i < BUCKETSIZE; i++) {
		fread(&detect, sizeof(RECORD), 1, fp);
		if(*detect.firstname == '\0') {
			fseek(fp, (-1L) * sizeof(RECORD), SEEK_CUR);
			fwrite(&temp, sizeof(RECORD), 1, fp);
			printf("Record: %s added to bucket %ld.\n", temp.firstname, hash);
			return;
		}
	}
	printf("Record %s not added! Bucket %ld full!\n", temp.firstname, hash);

	return;
}

void search_record(char* key, long hash, FILE* fp) {
	/*search_record: function called by search_or_insert that searches 
	in the hash table file for an existing record with the information
	entered.  Accepts the key (first name), that hashed to index, and file
	containing the hash table.  Returns nothing. */

	RECORD detect;
	int i;

	if(fseek(fp, hash * BUCKETSIZE * sizeof(RECORD), SEEK_SET) != 0) {
		printf("Fatal seek error!\n");
		exit(3);
	}

	for(i = 0; i < BUCKETSIZE; i++) { 
		fread(&detect, sizeof(RECORD), 1, fp);
		if(strcmp(detect.firstname, key) == 0) {
			printf("%s found in hash bucket %ld:\n", key, hash);
			printf("\t%s %s %8.2lf %4d\n", detect.firstname, detect.lastname, detect.amount, detect.student_id);
			return;
		}
	}
	printf("Record with key %s not found.\n", key);

	return;
}

void delete_record(char* key, long hash, FILE* fp) {
	/*delete_record: function called by search_or_insert that removes a record
	from the hash table file with the given information.  Accepts the first name,
	the hashed to index, and the file containing the hash table. Returns nothing. */

	RECORD detect, temp = {"","",0,0};
	int i;

	if(fseek(fp, hash * BUCKETSIZE * sizeof(RECORD), SEEK_SET) != 0) {
		printf("Fatal seek error!\n");
		exit(3);
	}

	for(i = 0; i < BUCKETSIZE; i++) { 
		fread(&detect, sizeof(RECORD), 1, fp);
		if(strcmp(detect.firstname, key) == 0) {
			fseek(fp, (-1L) * sizeof(RECORD), SEEK_CUR);
			fwrite(&temp, sizeof(RECORD), 1, fp);
			printf("Record: %s deleted from bucket %ld.\n", detect.firstname, hash);
			return;
		}
	}
	printf("Record not found, could not be deleted.\n");

	return;
}

/* OUTPUT TEST RUN

C:\Users\Eric\My Documents\Visual Studio 2010\Projects\Homework Assignment 3\Debug>start "" "Homework Assignment 3.exe" hashtable

INSERT RECORDS:
Enter student info (or type quit to go to menu): eric schultz 1234.56 1234
Record: ERIC added to bucket 1.
Enter student info (or type quit to go to menu): billy bob 1021343.23 123543
Must enter amount between 10.00 and 99,999.99.
Record not entered.

Enter student info (or type quit to go to menu): billy bob 99234.51 1432
Record: BILLY added to bucket 1.
Enter student info (or type quit to go to menu): tom cruise 87344.00 8732
Record: TOM added to bucket 10.
Enter student info (or type quit to go to menu): emily szeto 34592.98 7777
Record: EMILY added to bucket 7.
Enter student info (or type quit to go to menu): omg omg 1234.12 871236
Student ID must be 4 integers in length.
Record not entered.

Enter student info (or type quit to go to menu): omg omg 1234.12 7653
Record: OMG added to bucket 1.
Enter student info (or type quit to go to menu): hello there 840.85 2749
Record: HELLO added to bucket 7.
Enter student info (or type quit to go to menu): quit
Enter search, delete, add, or quit: search
SEARCH FOR RECORDS:
Enter student info (or type quit to go to menu): eric schultz 1234.56 1234
ERIC found in hash bucket 1:
        ERIC SCHULTZ  1234.56 1234
Enter student info (or type quit to go to menu): tom cruise 87344.00 8732
TOM found in hash bucket 10:
        TOM CRUISE 87344.00 8732
Enter student info (or type quit to go to menu): quit
Enter search, delete, add, or quit: delete
DELETE RECORDS:
Enter student info (or type quit to go to menu): eric schultz 1234.56 1234
Record: ERIC deleted from bucket 1.
Enter student info (or type quit to go to menu): quit
Enter search, delete, add, or quit: search
SEARCH FOR RECORDS:
Enter student info (or type quit to go to menu): eric schultz 1234.56 1234
Record with key ERIC not found.
Enter student info (or type quit to go to menu): quit
Enter search, delete, add, or quit: quit
*/
