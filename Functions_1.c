//Eric Schultz

#include <stdio.h>

float compute_tip(float);

int main (void)
{
	float dinner;
	float tip;

	printf("Insert total price of dinner: ");
	scanf("%f", &dinner);

	tip = compute_tip(dinner);

	printf("TIP:$ %3.2f", tip);
	printf("\n\nEric Schultz");

	return 0;
}

float compute_tip(float din)
{
    float tip;
	tip = din*0.2f;

	return (tip);
}

