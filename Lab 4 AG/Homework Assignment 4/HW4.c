/************************************************************

Homework Assignment 4
5/30/13

Author: Eric Schultz

Theater lighting settings by use of bit manipulation

************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int choiceChecker(char* sChoice);
void printLights(unsigned short lights);
void commandDriver (unsigned short* lights, int cmd);
void overlayer(unsigned short* lights, char* line);

int main ( void ) {

	char sChoice[100] = "";
	int choice = 0;
	unsigned short lights = 0;

	printf("1)  turn on all lights\n"
		   "2)  turn on center stage lights (lights 5-10)\n"
		   "3)  turn on left stage lights (lights 11-15)\n"
		   "4)  turn on right stage lights (lights 0-4)\n"
		   "5)  turn off all lights\n"
		   "6)  turn off center stage lights\n"
		   "7)  turn off left stage lights\n"
		   "8)  turn off right stage lights\n"
		   "9)  overlay on/off pattern onto light configuration\n"
		   "10) quit the menu\n"
		   "Enter command: ");

	while(choice != 10) {
		gets(sChoice);
		choice = choiceChecker(sChoice);
		if(choice != 0) {
			commandDriver(&lights, choice);
			printLights(lights);
		}
	}

	return 0;
}

int choiceChecker(char* sChoice) {
	/* function that takes user's entered line and searches it for errors
	or invalid entries.  Returns the command if entered correctly. */
	int choice;
	char** endPtr = NULL;
	char* token1 = "", *token2 = "";

	if(strcmp(sChoice, "") != 0) {
		token1 = strtok(sChoice, "\040\t");
		token2 = strtok(NULL, "\040\t");
		if(token2 != NULL) {
			printf("Please enter only one choice.\n");
			return 0;
		}
		choice = strtol(sChoice, endPtr, 10);
		if(choice < 1 || choice > 10) {
			printf("Invalid choice! Please select only 1 - 10.\n");
			return 0;
		}
		else {
			return choice;
		}
	}
	printf("Invalid entry. Enter new command: \n");

	return 0;
}

void commandDriver (unsigned short* lights, int cmd) {
	/* function that takes the current setting of lights and the user's
	input command and preforms the desired command on the lights.  Returns nothing */
	unsigned short mask = 0;
	char line[100] = "";

	if(cmd == 0) {
		printf("Entered invalid choice. Exiting program\n");
		exit(100);
	}
	if(cmd == 1) {
		//turn on all lights
		*lights = ~((unsigned short)0);
	}
	if(cmd == 2) {
		//turn on center lights
		mask = 2016;
		*lights = *lights | mask;
	}
	if(cmd == 3) {
		//turn on left lights
		mask = 63488;
		*lights = *lights | mask;
	}
	if(cmd == 4) {
		//turn on right lights
		mask = 31;
		*lights = *lights | mask;
	}
	if(cmd == 5) {
		//turn off all lights
		*lights = ((unsigned short)0);
	}
	if(cmd == 6) {
		//turn off center lights
		mask = 63519;
		*lights = *lights & mask;
	}
	if(cmd == 7) {
		//turn off left lights
		mask = 2047;
		*lights = *lights & mask;
	}
	if(cmd == 8) {
		//turn off right lights
		mask = 65504;
		*lights = *lights & mask;
	}
	if(cmd == 9) {
		//overlay pattern
		printf("Enter a light pattern and starting light: ");
		gets(line);
		overlayer(lights, line);
	}
	if(cmd == 10) {
		printf("Quitting...\n\n");
		system("pause");
		return;
	}
	return;
}

void printLights(unsigned short lights) {
	/* function that prints out the current lighting settings in 
	four sets of four bits each (binary).  Returns nothing. */
	int i = 0, j, a[16] = {0};

	while(lights > 0) {
		a[i] = lights % 2;
		lights = lights/2;
		++i;
	}

	printf("Current Light Settings: ");
	for(j = 15; j >= 0; --j) {
		printf("%d", a[j]);
		if((j % 4) == 0) {
			printf(" ");
		}
	}

	printf("\nEnter new command: ");

	return;
}

void overlayer(unsigned short* lights, char* line) {
	/* function used for command 9 that takes the current lighting
	settings and the user's entered commands, checks the line for errors,
	then changes the lighting settings according to the entered bit pattern
	and starting bit.  Returns nothing. */
	unsigned short mask = 0;
	int start_bit, pattern, p[16] = {0}, i = 0;
	char* numbits = "", *sStart_bit;
	char** endPtr = NULL;

	if(strcmp(line, "") == 0) {
		printf("Invalid entry.  Returning to menu.\n");
		return;
	}

	numbits = strtok(line, "\040\t");
	sStart_bit = strtok(NULL, "\040\t");
	pattern = strtol(numbits, endPtr, 2);
	start_bit = strtol(sStart_bit, endPtr, 10);

	if(pattern > 65535 || pattern < 0) {
		printf("Invalid pattern entered.  Returning to menu.\n"); 
		return;
	}
	if(start_bit > 15 || start_bit < 0) {
		printf("Invalid start light entered.  Returning to menu.\n"); 
		return;
	}
	if(strlen(numbits) + start_bit > 16) {
		printf("Inputted pattern and start bit combination too large.  Returning to menu.\n");
		return;
	}

    *lights &= ~(~((unsigned short) ~0 << strlen(numbits)) << start_bit);
    *lights |= pattern << start_bit;

	return;
}

/*    OUTPUT

1)  turn on all lights
2)  turn on center stage lights (lights 5-10)
3)  turn on left stage lights (lights 11-15)
4)  turn on right stage lights (lights 0-4)
5)  turn off all lights
6)  turn off center stage lights
7)  turn off left stage lights
8)  turn off right stage lights
9)  overlay on/off pattern onto light configuration
10) quit the menu
Enter command:
Invalid entry. Enter new command:
1
Current Light Settings: 1111 1111 1111 1111
Enter new command: 5
Current Light Settings: 0000 0000 0000 0000
Enter new command: 2
Current Light Settings: 0000 0111 1110 0000
Enter new command: 3
Current Light Settings: 1111 1111 1110 0000
Enter new command: 4
Current Light Settings: 1111 1111 1111 1111
Enter new command: 5
Current Light Settings: 0000 0000 0000 0000
Enter new command: 1
Current Light Settings: 1111 1111 1111 1111
Enter new command: 6
Current Light Settings: 1111 1000 0001 1111
Enter new command: 7
Current Light Settings: 0000 0000 0001 1111
Enter new command: 8
Current Light Settings: 0000 0000 0000 0000
Enter new command: 23
Invalid choice! Please select only 1 - 10.
12 45
Please enter only one choice.
2
Current Light Settings: 0000 0111 1110 0000
Enter new command: 9
Enter a light pattern and starting light: 10011 0
Current Light Settings: 0000 0111 1111 0011
Enter new command: 9
Enter a light pattern and starting light: 11 13
Current Light Settings: 0110 0111 1111 0011
Enter new command: 9
Enter a light pattern and starting light:
Invalid entry.  Returning to menu.
Current Light Settings: 0110 0111 1111 0011
Enter new command: 10
Quitting...

Press any key to continue . . .

*/